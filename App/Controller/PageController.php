<?php

namespace App\Controller;

use App\AppRepoManager;
use App\Controller\FormController;
use LidemCore\View;

class PageController
{
	public function index(): void
	{
		// Si identifié -> page list
		if (isset($_SESSION['role']) && ($_SESSION['role'] == 1 ||  $_SESSION['role'] == 2)) {
			header('Location: /list');
		}

		$view_data = [];


		if (!empty($_POST)) {
			$connexion = new ConnexionController;
			$view_data = $connexion->connexion();
		}

		$view = new View('pages/home');
		$view->title = 'Ajouter une annonce - Air DND';
		$view->render($view_data);
	}

	public function legalNotice(): void
	{
		$view = new View('pages/legal-notice');
		$view->title = 'Mentions illégalement illégales';

		$view->render();
	}

	public function listPage(): void
	{

		$view = new View('pages/list');

		if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {

			if (isset($_SESSION['id'])) {
				$view_data = [
					'h1_tag' => 'Nos Annonces',
					'rooms' => AppRepoManager::getRm()->getRoomRepo()->findByLodgerRooms()
				];
				$view->title = 'Nos annonces - Air DND';

				if (isset($_GET['reserved'])) {
					$view_data = [
						'h1_tag' => 'Vos réservations',
						'bookings' => AppRepoManager::getRm()->getBookingRepo()->findByLodgerRoomsReserved($_SESSION['id'])
					];
					$view = new View('pages/list');
					$view->title = 'Vos réservations - Air DND';
				}
			}
		} elseif (isset($_SESSION['role']) && $_SESSION['role'] == 2) {

			if (isset($_SESSION['id'])) {
				$view_data = [
					'h1_tag' => 'Vos Annonces',
					'rooms' => AppRepoManager::getRm()->getRoomRepo()->findByOwnerRooms($_SESSION['id'])
				];
				$view->title = 'Vos annonces - Air DND';

				if (isset($_GET['reserved'])) {
					$view_data = [
						'h1_tag' => 'Futur réservations',
						'bookings' => AppRepoManager::getRm()->getBookingRepo()->findByOwnerRoomsReserved($_SESSION['id'])
					];
					$view = new View('pages/list');
					$view->title = 'Futur réservations - Air DND';
				}
			}
		} else {
			header('Location: /');
		}

		$view->render($view_data);
	}

	public function detailPage(int $id)
	{

		$view_data = [
			'h1_tag' => 'Annonce',
			'rooms' => AppRepoManager::getRm()->getRoomRepo()->findByDetailRoom($id)
		];

		if (isset($_POST) && !empty($_POST)) {
			$view_data = [
				'insert' => AppRepoManager::getRm()->getBookingRepo()->addReservation($id)
			];

			header('Location: /list?reserved');
			exit();
		}

		$view = new View('pages/detail');
		$view->title = 'Détail - Air DND';
		$view->render($view_data);
	}
	public function addAnnoncePage()
	{

		if ($_SESSION['role'] == 2) {

			$FormController = new FormController;
			$FormController->addAnnoncePage();
		} else {
			header('Location: /');
		}
	}

	public function createAccount()
	{
		if(empty($_SESSION)){
			$FormController = new FormController;
			$FormController->createAccount();
		}
	}

	public function deconnexion()
	{
		session_destroy();
		header('Location: /');
	}
}
