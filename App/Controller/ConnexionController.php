<?php

namespace App\Controller;

use App\App;
use App\AppRepoManager;
use ErrorException;
use Exception;
use LidemCore\View;

class ConnexionController
{
    public static function valid_donnees(string $donnees)
    {
        $donnees = trim($donnees);
        $donnees = stripslashes($donnees);
        $donnees = htmlspecialchars($donnees);
        return $donnees;
    }


    public function connexion():array
    {

        // Petit flag pour vérifier si tout est bon, si $error = true il y a un problème
        $error = false;

        //Test des valeurs reçu
        $_POST['mail'] != null ? $mail = $this->valid_donnees($_POST['mail']) : $error = true;
        $_POST['password'] != null ? $password = $this->valid_donnees($_POST['password']) : $error = true;

        if ($error === false && filter_var($mail, FILTER_VALIDATE_EMAIL) && $password) {
            // si pas d'érreur on test si la query existe
            $view_data = [
                'connexion' => AppRepoManager::getRm()->getUserRepo()->userExist(),
            ];
            if($view_data['connexion'] === null){
                $view_data=[
                    'error_form' => 'Identifiant ou mot de passe incorrect'
                ];
            }

        } else {
            $view_data = [
                'error_form' => 'Veuillez remplir les champs correctements'
            ];
        }

        return $view_data;
    }
}
