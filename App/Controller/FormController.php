<?php

namespace App\Controller;

use App\App;
use App\AppRepoManager;
use ErrorException;
use Exception;
use LidemCore\View;

class FormController
{

    public static function valid_donnees(string $donnees)
    {
        $donnees = trim($donnees);
        $donnees = stripslashes($donnees);
        $donnees = htmlspecialchars($donnees);
        return $donnees;
    }

    /**
     *
     * Vérifie si la donnée est conforme au type 
     * Renvoie False si elle ne correspond pas
     */
    public static function valid_mail_donnees($donnees, $type)
    {
        $type = strtoupper($type);
        filter_var($donnees, FILTER_VALIDATE_EMAIL);
        return $donnees;
    }

    public function addAnnoncePage()
    {
        $view_data = [
            'h1_tag' => 'Annonce',
            'equipments' => AppRepoManager::getRm()->getEquipmentRepo()->findAll()
        ];

        if (!empty($_POST)) {
            $error = false;

            $_POST['description'] != null ?: $error = true;
            new ErrorException("Description non valide");
            $_POST['country'] != null ?: $error = true;
            new ErrorException("country non valide");
            $_POST['city'] != null ?: $error = true;
            new ErrorException("city non valide");

            $_POST['surface'] != null && floatval($_POST['surface']) ?: $error = true;
            new ErrorException("surface non valide");
            $_POST['price'] != null && floatval($_POST['price']) ?: $error = true;
            new ErrorException("price non valide");

            $_POST['room_type'] != null && intval($_POST['room_type']) ?: $error = true;
            new ErrorException("room_type non valide");
            $_POST['capacity'] != null && intval($_POST['capacity']) ?: $error = true;
            new ErrorException("capacity non valide");

            isset($_POST['equipment']) && $_POST['equipment'] != null ?: $error = true;

            if ($error === false) {

                $view_data = [
                    'h1_tag' => 'test',
                    'equipments' => AppRepoManager::getRm()->getEquipmentRepo()->findAll(),
                    'insertAdrress' => AppRepoManager::getRm()->getAddressRepo()->addAddress(),
                    'insertRoom' => AppRepoManager::getRm()->getRoomRepo()->addRoomAnnonce(),
                    'insertEquipment' => AppRepoManager::getRm()->getEquipmentRepo()->addEquipmentByRoom()
                ];

                header('Location: /list');
                exit();
            } else {

                $view_data = [
                    'h1_tag' => 'Annonce',
                    'error_form' => 'Veuillez remplir les champs correctements',
                    'equipments' => AppRepoManager::getRm()->getEquipmentRepo()->findAll()
                ];
            }
        }


        $view = new View('pages/add-annonce');
        $view->title = 'Ajouter une annonce - Air DND';
        $view->render($view_data);
    }

    public function createAccount()
    {


        $view_data = [
            'h1_tag' => 'Creation d\'un compte'
        ];
        if (isset($_POST) && !empty($_POST)) {

            var_dump($_POST);
            $error = false;

            $nickname = $this->valid_donnees($_POST["nickname"]);
            $password = $this->valid_donnees($_POST["password"]);
            $mail = $this->valid_donnees($_POST["mail"]);

            /*Si les champs prenom et mail ne sont pas vides et si les donnees ont*bien la forme attendue...*/
            if (
                !empty($nickname)
                && strlen($nickname) <= 20
                && !empty($password)
                && !empty($mail)
                && filter_var($mail, FILTER_VALIDATE_EMAIL)
                && isset($_POST['user_role'])
                && intval($_POST['user_role'])
            ) {
                if (AppRepoManager::getRm()->getUserRepo()->accountAlreadyExist($mail) === true) {
                    $view_data = [
                        'error_form' => 'cette addresse mail est déjà utilisée'
                    ];
                } else {

                    $view_data = [
                        'newAccount' => AppRepoManager::getRm()->getUserRepo()->newUser(),
                    ];
                    header('Location: /');
                    exit;
                }
            } else {
                $view_data = [
                    'h1_tag' => 'Creation d\'un compte',
                    'error_form' => 'Veuillez remplir les champs correctements',
                ];
            }
        }



        $view = new View('pages/create-account');
        $view->title = 'Créer un compte - Air DND';
        $view->render($view_data);
    }
}
