<?php

namespace App;

use App\Controller\FormController;
use MiladRahimi\PhpRouter\Exceptions\InvalidCallableException;
use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
use MiladRahimi\PhpRouter\Router;

use App\Controller\PageController;
use App\Controller\ToyController;
use LidemCore\Database\DatabaseConfigInterface;
use LidemCore\View;

class App implements DatabaseConfigInterface
{
	private const DB_HOST = 'database';
	private const DB_NAME = 'lamp';
	private const DB_USER = 'lamp';
	private const DB_PASS = 'lamp';

	private static ?self $instance = null;
	public static function getApp(): self

	{
		if( is_null( self::$instance )) self::$instance = new self();

		return self::$instance;
	}

	private Router $router;

	private function __construct() {
		$this->router = Router::create();
	}

	public function getHost(): string
	{
		return self::DB_HOST;
	}

	public function getName(): string
	{
		return self::DB_NAME;
	}

	public function getUser(): string
	{
		return self::DB_USER;
	}

	public function getPass(): string
	{
		return self::DB_PASS;
	}

	public function start(): void
	{
		$this->registerRoutes();
		$this->startRouter();
	}

	private function registerRoutes(): void
	{
		// Déclaration des patterns pour tester les valeurs des arguments
		$this->router->pattern( 'id', '[1-9]\d*' );
		$this->router->pattern( 'slug', '[a-z-\d]+' );

		$this->router->any( '/', [ PageController::class, 'index' ] );
		$this->router->any( '/createAccount', [ PageController::class, 'createAccount']);
		$this->router->get('/list', [PageController::class, 'listPage']);
		$this->router->any('/show/{id}', [PageController::class, 'detailPage']);
		$this->router->any('/add-annonce', [PageController::class, 'addAnnoncePage']);

		$this->router->get( '/deco', [ PageController::class, 'deconnexion']);

		// $this->router->get( '/jouet/{id}', [ ToyController::class, 'show' ] );
		$this->router->get( '/jouet/{slug}', [ ToyController::class, 'showBySlug' ] );
	}

	private function startRouter(): void
	{
		try {
			$this->router->dispatch();
		}
		catch( RouteNotFoundException $e ) {
			View::renderError();
		}
		catch( InvalidCallableException $e ) {
			View::renderError( 500 );
		}
	}

	private function __clone() {}
	private function __wakeup() {}
}