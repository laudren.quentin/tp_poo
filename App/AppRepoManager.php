<?php

namespace App;

use App\Model\Repository\AddressRepository;
use App\Model\Repository\BookingRepository;
use App\Model\Repository\RoomRepository;
use App\Model\Repository\ToyRepository;
use App\Model\Repository\UserRepository;
use App\Model\Repository\EquipmentRepository;
use LidemCore\RepositoryManagerTrait;

class AppRepoManager
{
	use RepositoryManagerTrait;

	private BookingRepository $BookingRepository;
	public function getBookingRepo(): BookingRepository { return $this->BookingRepository; }
	
	private RoomRepository $RoomRepository;
	public function getRoomRepo(): RoomRepository { return $this->RoomRepository; }

	private ToyRepository $toyRepository;
	public function getToyRepo(): ToyRepository { return $this->toyRepository; }
	
	private EquipmentRepository $EquipmentRepository;
	public function getEquipmentRepo(): EquipmentRepository { return $this->EquipmentRepository; }

	private UserRepository $UserRepository;
	public function getUserRepo(): UserRepository { return $this->UserRepository; }

	private AddressRepository $AddressRepository;
	public function getAddressRepo(): AddressRepository { return $this->AddressRepository; }



	protected function __construct()
	{
		$config = App::getApp();

		$this->BookingRepository = new BookingRepository( $config );
		$this->toyRepository = new ToyRepository( $config );
		$this->RoomRepository = new RoomRepository( $config );
		$this->UserRepository = new UserRepository( $config );
		$this->EquipmentRepository = new EquipmentRepository( $config );
		$this->AddressRepository = new AddressRepository( $config );
	}
}