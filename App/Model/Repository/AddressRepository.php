<?php

namespace App\Model\Repository;

use App\Controller\FormController;
use App\Model\Address;
use App\Model\Room;
use LidemCore\Repository;

class AddressRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'rooms';
	}

	public function findAll(): array
	{
		return $this->readAll(Room::class);
	}

	public function findById(int $id): ?Room
	{
		return $this->readById(Room::class, $id);
	}

	public function findBySlug(string $slug): ?Room
	{
		$q = sprintf('SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName());

		$sth = $this->pdo->prepare($q);

		if (!$sth) return null;

		$sth->execute(['slug' => $slug]);

		$row_data = $sth->fetch();

		return !empty($row_data) ? new Room($row_data) : null;
	}

	public function addAddress()
	{
		$country = FormController::valid_donnees($_POST['country']);
		$city = FormController::valid_donnees($_POST['city']);

		$q = 'INSERT INTO addresses (country, city)
			VALUES ( :country , :city);';

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;

		$sth->execute([
			'country' => $country,
			'city' => $city
		]);
		
	}
}
