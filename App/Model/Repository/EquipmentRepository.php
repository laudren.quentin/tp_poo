<?php

namespace App\Model\Repository;

use App\App;
use App\Model\Equipment;
use LidemCore\Database\Database;
use LidemCore\Repository;

class EquipmentRepository extends Repository
{
	protected function getTableName(): string { return 'equipments'; }

	public function findAll(): array
	{
		return $this->readAll( Equipment::class );
	}

	public function findById( int $id ): ?Equipment
	{
		return $this->readById( Equipment::class, $id );
	}

	public function findBySlug( string $slug ): ?Equipment
	{
		$q = sprintf( 'SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName() );

		$sth = $this->pdo->prepare( $q );

		if( !$sth ) return null;

		$sth->execute( [ 'slug' => $slug ] );

		$row_data = $sth->fetch();

		return !empty( $row_data ) ? new Equipment( $row_data ) : null;

	}

	public function addEquipmentByRoom()
	{
		$room_id = Database::getPDO(App::getApp())->lastInsertId();

		foreach ($_POST['equipment'] as $equipment){
			
			$q = 'INSERT INTO equipments_links (room_id, equipment_id)
			VALUES ( :room_id , :equipment_id);';
			
			$sth = $this->pdo->prepare($q);
			if (!$sth) return null;

			$sth->execute([
				'room_id' => $room_id,
				'equipment_id' => $equipment
			]);
			
		}
	}

}