<?php

namespace App\Model\Repository;

use App\Model\Address;
use App\Model\Booking;
use App\Model\Room;
use LidemCore\Repository;

class BookingRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'bookings';
	}

	public function findAll(): array
	{
		return $this->readAll(Booking::class);
	}

	public function findById(int $id): ?Booking
	{
		return $this->readById(Booking::class, $id);
	}

	public function findBySlug(string $slug): ?Booking
	{
		$q = sprintf('SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName());

		$sth = $this->pdo->prepare($q);

		if (!$sth) return null;

		$sth->execute(['slug' => $slug]);

		$row_data = $sth->fetch();

		return !empty($row_data) ? new Booking($row_data) : null;
	}

	public function findByLodgerRoomsReserved(int $id)
	{

		$q = 'SELECT r.*, a.city , a.country,
			b.user_id, b.room_id, b.check_in, b.check_out
			FROM bookings AS b
			INNER JOIN rooms AS r ON r.id = b.room_id
			INNER JOIN addresses AS a ON r.address_id = a.id
			WHERE b.user_id = :id
			ORDER BY b.check_in ASC;';

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		$sth->execute(['id' => $id]);

		$row_data = [];
		while ($row = $sth->fetch()) {
			if (!empty($row)) {
				$booking = new Booking($row);
				$booking->room = new Room($row);
				$booking->room->id = $booking->room_id;

				$booking->room->address = new Address($row);
				$booking->room->address->id = $booking->room->address_id;

				$row_data[] = $booking;
			}
		}
		return $row_data;
	}

	public function findByOwnerRoomsReserved(int $id)
	{

		$q = 'SELECT r.*, a.city , a.country,
			b.user_id, b.room_id, b.check_in, b.check_out
			FROM bookings AS b
			INNER JOIN rooms AS r ON r.id = b.room_id
			INNER JOIN addresses AS a ON r.address_id = a.id
			WHERE r.owner_id = :id
			ORDER BY b.check_in ASC;';

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		$sth->execute(['id' => $id]);

		$row_data = [];
		while ($row = $sth->fetch()) {
			if (!empty($row)) {
				$booking = new Booking($row);
				$booking->room = new Room($row);
				$booking->room->id = $booking->room_id;

				$booking->room->address = new Address($row);
				$booking->room->address->id = $booking->room->address_id;

				$row_data[] = $booking;
			}
		}
		return $row_data;
	}

	public function addReservation($id)
	{
		$user_id = $_SESSION['id'];
		$room_id = $id;
		$check_in = $_POST['check_in'] ;
		$check_out = $_POST['check_out'];

		if( strval($check_in) && strval($check_out) && intval($room_id) && intval($user_id) ){
			
			$q = 'INSERT INTO bookings (user_id, room_id, check_in, check_out)
			VALUES ('.$user_id.', '. $room_id .', \''. $check_in .'\' , \''. $check_out .'\');';
			$sth = $this->pdo->prepare($q);
			if (!$sth) return null;
			$sth->execute();
			
		}

	}
}
