<?php

namespace App\Model\Repository;

use App\Controller\ConnexionController;
use App\Model\User;
use ErrorException;
use LidemCore\Repository;

class UserRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'users';
	}

	public function findAll(): array
	{
		return $this->readAll(User::class);
	}

	public function findById(int $id): ?User
	{
		return $this->readById(User::class, $id);
	}

	public function findBySlug(string $slug): ?User
	{
		$q = sprintf('SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName());

		$sth = $this->pdo->prepare($q);

		if (!$sth) return null;

		$sth->execute(['slug' => $slug]);

		$row_data = $sth->fetch();

		return !empty($row_data) ? new User($row_data) : null;
	}

	public function userExist(): ?user
	{
		$password = ConnexionController::valid_donnees($_POST['password']);
		$mail = ConnexionController::valid_donnees($_POST['mail']);


		$q = 'SELECT * FROM users WHERE mail = :mail AND `password` = :pass ;';
		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;

		$sth->execute([
			'mail' => $mail,
			'pass' => $password
		]);

		$user_data = $sth->fetch();
		if (!$user_data) {
			$user = null;
		} else {
			$user = new User($user_data);
			$_SESSION['role'] = $user->user_role;
			$_SESSION['id'] = $user->id;
			$_SESSION['name'] = $user->nickname;
		}


		return $user;
	}

	public function newUser()
	{
		$nickname = $_POST['nickname'];
		$password = $_POST['password'];
		$mail = $_POST['mail'];
		$user_role = intval($_POST['user_role']);


		$q = 'INSERT INTO users (nickname, password, mail, user_role)
			VALUES (:nickname, :pass, :mail, :user_role);';
		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		$sth->execute([
			'nickname' 	=> $nickname,
			'pass' 		=> $password,
			'mail' 		=> $mail,
			'user_role' => $user_role

		]);
	}

	public function accountAlreadyExist($mail):bool
	{
		$q = 'SELECT * FROM users WHERE mail = :mail;';

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;

		$sth->execute([
			'mail' => $mail,
		]);

		$user_data = $sth->fetch();
		if (!$user_data) {
			return false;
		} else {
			return true;
		}

	}
}
