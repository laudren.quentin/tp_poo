<?php

namespace App\Model\Repository;

use App\Model\Toy;
use LidemCore\Repository;

class ToyRepository extends Repository
{
	protected function getTableName(): string { return 'toys'; }

	public function findAll(): array
	{
		return $this->readAll( Toy::class );
	}

	public function findById( int $id ): ?Toy
	{
		return $this->readById( Toy::class, $id );
	}

	public function findBySlug( string $slug ): ?Toy
	{
		$q = sprintf( 'SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName() );

		$sth = $this->pdo->prepare( $q );

		if( !$sth ) return null;

		$sth->execute( [ 'slug' => $slug ] );

		$row_data = $sth->fetch();

		return !empty( $row_data ) ? new Toy( $row_data ) : null;

	}

}