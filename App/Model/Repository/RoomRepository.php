<?php

namespace App\Model\Repository;

use App\App;
use App\Controller\FormController;
use App\Model\Address;
use App\Model\Equipment;
use App\Model\Equipment_link;
use App\Model\Room;
use LidemCore\Database\Database;
use LidemCore\Repository;

class RoomRepository extends Repository
{
	protected function getTableName(): string
	{
		return 'rooms';
	}

	public function findAll(): array
	{
		return $this->readAll(Room::class);
	}

	public function findById(int $id): ?Room
	{
		return $this->readById(Room::class, $id);
	}

	public function findBySlug(string $slug): ?Room
	{
		$q = sprintf('SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName());

		$sth = $this->pdo->prepare($q);

		if (!$sth) return null;

		$sth->execute(['slug' => $slug]);

		$row_data = $sth->fetch();

		return !empty($row_data) ? new Room($row_data) : null;
	}

	public function findByLodgerRooms()
	{

		$q = 'SELECT r.*, a.city , a.country
			FROM rooms AS r
			INNER JOIN addresses AS a ON r.address_id = a.id;';

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		$sth->execute();
		$row_data = [];
		while ($row = $sth->fetch()) {

			$room = new Room($row);
			$room->address = new Address($row);
			$room->address->id = $room->address_id;

			$row_data[] = $room;
		}
		return $row_data;
	}
	public function findByOwnerRooms(int $id)
	{

		$q = 'SELECT r.*, a.city , a.country
			FROM rooms AS r
			INNER JOIN addresses AS a ON r.address_id = a.id
			WHERE r.owner_id = :id ;';

		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		$sth->execute(['id' => $id]);

		$row_data = [];
		while ($row = $sth->fetch()) {
			if (!empty($row)) {
				$room = new Room($row);
				$room->address = new Address($row);
				$room->address->id = $room->address_id;

				$row_data[] = $room;
			}
		}
		return $row_data;
	}

	public function findByDetailRoom(int $id_room)
	{
		/* Requête de la liste d'equipement d'une chambre */
		$q_equipment = 'SELECT r.*, el.*, e.equipment
		FROM rooms AS r
		JOIN equipments_links AS el ON r.id = el.room_id
		JOIN equipments AS e ON el.equipment_id = e.id
		WHERE r.id = :id ;';

		$sth_equipment = $this->pdo->prepare($q_equipment);
		if (!$sth_equipment) return null;
		$sth_equipment->execute(['id' => $id_room]);

		$equipment_data = [];
		while ($row = $sth_equipment->fetch()) {
			if (!empty($row)) {
				$equipment = new Equipment($row);

				$equipment_data[] = $equipment;
			}
		}

		/* Requête pour trouver la chambre */
		$q_room = 'SELECT r.*, a.city , a.country
			FROM rooms AS r
			INNER JOIN addresses AS a ON r.address_id = a.id
			WHERE r.id = :id ;';

		$sth_room = $this->pdo->prepare($q_room);
		if (!$sth_room) return null;
		$sth_room->execute(['id' => $id_room]);



		$room_data = $sth_room->fetch();
		$room = new Room($room_data);
		$room->address = new Address($room_data);
		$room->address->id = $room->address_id;

		/* Ajout du tableau Equipement à L'objet Room */
		$room->equipment = $equipment_data;

		$row_data[] = $room;

		return $row_data;
	}

	public function addRoomAnnonce()
	{

		$address_id = $this->pdo->lastInsertId();
		$address_id = intval($address_id);

		$description = FormController::valid_donnees($_POST['description']);
		$surface = $_POST['surface'];
		$price = $_POST['price'];
		$room_type = $_POST['room_type'];
		$capacity = $_POST['capacity'];

		$q = 'INSERT INTO rooms (description, surface, price, room_type, capacity, owner_id, address_id)
			VALUES ( :description , :surface, :price, :room_type, :capacity, :owner_id, :address_id );';
			
		$sth = $this->pdo->prepare($q);
		if (!$sth) return null;
		
		$sth->execute([
			'description' =>$description,
			'surface' => $surface,
			'price' => $price,
			'room_type' => $room_type,
			'capacity' => $capacity,
			'owner_id' => $_SESSION['id'],
			'address_id' => $address_id
		]);


	}
}
