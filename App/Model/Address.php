<?php

namespace App\Model;

use LidemCore\Model;

class Address extends Model
{
	public int $id;

	public string $country;
    public string $city;

}