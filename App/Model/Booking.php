<?php

namespace App\Model;

use LidemCore\Model;

class Booking extends Model
{
    public int $id;

    public int $user_id;
    public int $room_id;

    public string $check_in;
    public string $check_out;

    public ?Room $room = null;

    public function getDataDMY(string $date): string
    {
        //Convertit la date de 'yyyy-mm-dd' acceptable pour mysql
        $date = date('d-m-Y', strtotime($date));
        return $date;
    }
}
