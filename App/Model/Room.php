<?php

namespace App\Model;

use LidemCore\Model;

class Room extends Model
{

    public int $id;
    public int $surface;
    public int $capacity;
    public int $owner_id;
    public int $address_id;
    public int $room_type;

    public float $price;
    public string $description;

    public function getType(int $room_type): string
    {
        switch ($room_type) {
            case 1:
                return 'Logement Entier';
                break;
            case 2:
                return 'Chambre Privée';
                break;

            case 3:
                return 'Chambre Partagée';
                break;
            default:
                return '';
                break;
        }
    }

    public ?Address $address = null;
}
