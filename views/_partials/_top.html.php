<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/assets/css/style.css">

	<title><?php echo $title_tag ?></title>
</head>
<body>
	<header>
		<a href="/" id="logo">AIR DND</a>
		<nav>
		<?php if ( empty($_SESSION)): ?>
		<div> <!-- SANS SESSION VALIDE -->
			<a href="/createAccount">Créer un compte</a>
			<form action="" method="POST">
				<input type="text" placeholder="mail" name="mail">
				<input type="password" placeholder="pass" name="password">
				<input type="submit" value="se connecter">
			</form>
		</div>
		<?php endif ?>
		<?php if( isset($_SESSION['role']) && $_SESSION['role'] == 1): ?>
		<div> <!-- LODGER SESSION -->
			<a href="/list">Nos Annonces</a>
			<a href="/list?reserved">Mes Réservations</a>
			<a href="/deco">Se déconnecter</a>
		</div>
		<?php endif ?>
		<?php if( isset($_SESSION['role']) && $_SESSION['role'] == 2): ?>
			<div> <!-- Owner SESSION -->
			<a href="/list">Mes Annonces</a>
			<a href="/list?reserved">Futur Réservations</a>
			<a href="/add-annonce">Ajouter une Annonce</a>
			<a href="/deco">Se déconnecter</a>
		</div>
		<?php endif ?>

		</nav>
	</header>

	<main>

