<h1> <?php echo $title_tag ?></h1>

<form action="" method="post" class="formAddAnnonce">

    <div>
        <label for="description">Description:</label>
        <textarea id="description" name="description" rows="5" cols="33"></textarea>
    </div>
    <div>
        <label for="surface">Surface:</label>
        <input type="text" name="surface" id="surface">
    </div>
    <div>
        <label for="capacity">Nombre de couchage:</label>
        <input type="text" name="capacity" id="capacity">
    </div>
    <div>
        <label for="price">Prix:</label>
        <input type="number" name="price" id="price" min="0" step="0.01">
    </div>
    <div>    
        <label for="room_type_select">Type de logement:</label>
        <select name="room_type" id="room_type_select">
            <option value="">--Veuillez selectionner un type--</option>
            <option value="1"> Logement entier </option>
            <option value="2"> Chambre partagée </option>
            <option value="3"> Chambre privée </option>
        </select>
    </div>

    <div>
        <label for="country">Pays:</label>
        <input type="text" name="country" id="country">
    </div>
    <div>
        <label for="city">Ville:</label>
        <input type="text" name="city" id="city">
    </div>

    <?php if (isset($equipments)) : ?>
        <?php if (empty($equipments)) : ?>
        <?php else : ?>
            <fieldset>
                <legend>Equipements:</legend>
                <?php foreach ($equipments as $equipment) : ?>
                    <div>
                        <input type="checkbox" id="<?php echo $equipment->equipment ?>" name="equipment[]" value=" <?php echo $equipment->id ?>">
                        <label for="<?php echo $equipment->equipment ?>"><?php echo $equipment->equipment ?></label>
                    </div>
                <?php endforeach ?>
            </fieldset>
        <?php endif ?>
    <?php endif ?>

    <input type="submit" value="ajouter">
</form>

<?php if( isset($error_form) ): ?>
    <p style="color:red;"> <?php echo $error_form ?> </p>
<?php endif ?>
