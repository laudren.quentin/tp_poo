<h1><?php echo $h1_tag ?></h1>

<!-- Page liste Menu -->
<?php if (isset($rooms)) : ?>
    <?php if (empty($rooms)) : ?>
        <div>Aucunne annonce</div>
    <?php else : ?>
        <ul>
            <?php foreach ($rooms as $room) : ?>
                <li>
                    <a href="/show/<?php echo $room->id ?>" class="listCard">
                        <p class="h4"><?php echo $room->description ?></p>
                        <div>
                            <p>Prix: <?php echo $room->price ?></p>
                            <p>Pays: <?php echo $room->address->country ?></p>
                            <p>Ville: <?php echo $room->address->city ?></p>
                        </div>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    <?php endif ?>
<?php endif ?>

<!-- Page liste réservation -->
<?php if (isset($bookings)) : ?>
    <?php if (empty($bookings)) : ?>
        <div>Aucunne réservation</div>
    <?php else : ?>
        <ul>
            <?php foreach ($bookings as $booking) : ?>
                <li>
                    <a href="/show/<?php echo $booking->room->id ?>" class="listCard">
                        <p class="h4"><?php echo $booking->room->description ?></p>
                        <div>
                            <p>Prix: <?php echo $booking->room->price ?></p>
                            <p>Pays: <?php echo $booking->room->address->country ?></p>
                            <p>Ville: <?php echo $booking->room->address->city ?></p>
                        </div>
                        <div>
                            <p>Date d'arrivée: <?php echo $booking->getDataDMY($booking->check_in) ?></p>
                            <p>Date de départ: <?php echo $booking->getDataDMY($booking->check_out) ?></p>
                        </div>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    <?php endif ?>
<?php endif ?>