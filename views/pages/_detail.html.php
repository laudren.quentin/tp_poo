<h1><?php echo $h1_tag ?></h1>

<!-- Page Detail -->
<?php if (isset($rooms)) : ?>
    <?php if (empty($rooms)) : ?>
        <div>Cette chambre n'existe pas</div>
    <?php else : ?>
        <?php foreach ($rooms as $room) : ?>
            <div>
                <h4><?php echo $room->description ?></h4>
                <div>
                    <h5>Infos utiles:</h5>
                    <p>Type de logement: <?php echo $room->getType($room->room_type) ?></p>
                    <p>Nombre de couchage: <?php echo $room->capacity ?></p>
                    <p>Surface: <?php echo $room->surface ?> m2</p>
                    <p>Prix: <?php echo $room->price ?>€</p>
                    <p>Pays: <?php echo $room->address->country ?></p>
                    <p>Ville: <?php echo $room->address->city ?></p>

                    <h5>Equipements:</h5>
                    <ul>
                        <?php foreach($room->equipment as $equipment): ?>
                        <li><?php echo $equipment->equipment ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <?php if($_SESSION['role'] == 1): ?>
                <!-- Formulaire de réservation si Lodger -->
                <h5>Réservation</h5>
                        
                <form action="" method="post">
                    <div>
                        <label for="check_in">Date d'arriver: </label>
                        <input type="date" name="check_in" id="check_out" required="required">
                    </div>
                    <div>
                        <label for="check_out">Date de départ: </label>
                        <input type="date" name="check_out" id="check_out" required="required">
                    </div>

                    <input type="submit" value="Réserver">
                </form>
                <?php endif ?>
            </div>
        <?php endforeach ?>
    <?php endif ?>
<?php endif ?>