<h1><?php echo $title_tag ?></h1>
<?php if( isset($error_form) ): ?>
    <p style="color:red;"> <?php echo $error_form ?> </p>
<?php endif ?>
<form action="" method="POST">
    <div>
        <label for="nickname">Nom:</label>
        <input type="text" name="nickname" id="nickname">
    </div>
    <div>
        <label for="mail">Email:</label>
        <input type="email" name="mail" id="mail">
    </div>
    <div>
        <label for="password">Password</label>
        <input type="password" id="password" name="password">
    </div>

    <fieldset>
        <legend>Role</legend>
        <input type="radio" id="annonceur" name="user_role" value="2">
        <label for="annonceur">Annonceur</label>

        <input type="radio" id="user" name="user_role" value="1">
        <label for="user">Utilisateur</label>
    </fieldset>

    <input type="submit" value="Créer un compte">

</form>